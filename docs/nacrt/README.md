# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | 1. Bogdan Petrović, 2. Aleksandar Cuculoski, 3. Maja Nikoloska in 4. Martin Arsovski  |
| **Kraj in datum** | Ljubljana, 20.04.2019  |



## Povzetek


* Načrt sistema je organiziran tako da razvojna ekipa ima čim boljši vpogled v strukturo, arhitekturo, razrede in metode ki bodo vključene med tekom implementacije.
* Arhitektura je narejena po MVC vzorcu. Komponente kot so logike za obdelavo uporabniških informacij, različni tipi informacij v podatkovni bazi in različni pogledi skupaj sodelujejo v namen popolne funkcionalnosti krmilnika, modela in uporabniškega vmesnika. Varnost je tudi pomemen del aplikacije in se bomo potrudili narediti zanesljivo šifriranje uporabniških gesel.
* Obstaja več tipov uporabnikov kot na primer študent, profesor in asistent. Zato smo v načrt uključili več razredov ki dedujejo atribute od univerzalnega razreda Uporabnik.  
* Potrudili smo se narediti načrt obnašanja čim bolj enostavno in sistematično. Razmislili smo, kaj bi bil najbolj optimalen način realizacije vseh tipov tokov in smo se glede na tisto odločbo lotili problema obnašanja.



## 1. Načrt arhitekture


![Logični pogled arhitekture](../img/logPogledN.png)

![Razvojni pogled 1.](../img/razpogN.png)

![Razvojni pogled 2.](../img/razPogledNw2.png)

* Za prikaz uporabite enostavne prikaze, kot so blokovni ali paketni diagrami. Uporabite lahko arhitekturne vzorce.




## 2. Načrt strukture

![Strukture](../img/strukture.png)

## 3. Načrt obnašanja
* Akcije med akterji in objekti znotraj sistema smo predstavili z uporabo dijagrama zaporedja.
* Sodelujoči objekti in akterji so navedeni na vrhu diagrama, od njih navzdol pa je narisana navpična črtkana črta.
* Interakcije med objekti so označene z usmerjenimi in opisanimi puščicami.

#### 1. Prijava v sistem  

| ![Prva funk](../img/1.png) |
|:--:|
| *Osnovni in izjemni tok, prijava v sistem* |

#### 2. Pregled predmetov
| ![Vtora funk](../img/2.png) |
|:--:|
| *Osnovni in izjemni tok pregled predmetov* |

#### 3. Pregled "TO-DO" seznama
| ![Treta funk](../img/3.png) |
|:--:|
| *Osnovni, alternativni in izjemni tok pregled "TO-DO" seznama* |

#### 4. Generiranje urnika
| ![Cetvrta funk](../img/4.png) |
|:--:|
| *Osnovni in izjemni tok generiranje urnika* |

#### 5. Spreminjanje urnika

##### Študent
| ![Petta funk A](../img/5student.png) |
|:--:|
| *Osnovni, alternativni in izjemni tok spreminjanje urnika* |

##### Urničar
| ![Petta funk B](../img/5urnicar.png) |
|:--:|
| *Osnovni, alternativni in izjemni tok spreminjanje urnika* |

#### 6. Objavljanje rokov sprotnih obveznosti

##### Asistent
| ![Sesta funk A](../img/6asistent.png) |
|:--:|
| *Osnovni in izjemni tok objavljanje rokov sprotnih obveznosti* |

##### Profesor
| ![Sesta funk B](../img/6profesor.png) |
|:--:|
| *Osnovni in izjemni tok objavljanje rokov sprotnih obveznosti* |


#### 7. Spreminjanje rokov sprotnih obveznosti 

##### Asistent
| ![Sedma funk B](../img/7asistent.png) |
|:--:|
*Osnovni, alternativni in izjemni tok spreminjanje rokov sprotnih obveznosti*

##### Profesor
| ![Sedma funk B](../img/7profesor.png) |
|:--:|
*Osnovni, alternativni in izjemni tok spreminjanje rokov sprotnih obveznosti*

#### 8. Objava rezultatov 
##### Asistent
| ![Osma funk B](../img/8asistent.png) |
|:--:|
*Osnovni in izjemni tok objava rezultatov*

##### Profesor
| ![Osma funk B](../img/8profesor.png) |
|:--:|
*Osnovni in izjemni tok objava rezultatov*

#### 9.Dodajanje termina za govorilne ure

##### Asistent/Profesor

| ![Dodajanje termina za govorilne ure - asistent_profesor](../img/DodajanjeTerminAsistentProfesor.png) | 
|:--:| 
| *Osnovni in izjemni tok dodajanja termina za govorilne ure - asistent in profesor* |

##### Študent

| ![Dodajanje termina za govorilne ure student](../img/DodajanjeTerminStudent.png) | 
|:--:| 
| *Osnovni in izjemni tok dodajanja termina za govorilne ure - študent* |

#### 10.Dodajanje opis obveznostih

| ![Dodajanje opis obveznostih](../img/DodajanjeOpis.png) | 
|:--:| 
| *Osnovni in izjemni tok dodajanja opisa obveznostih* |

#### 11.Dodajanje prioritete obveznostih

| ![Dodajanje prioritete obveznostih](../img/DodajanjePrioritete.png) | 
|:--:| 
| *Osnovni in izjemni tok dodajanja prioritete obveznostih* |

#### 12.Spreminjanje prioritete obveznostih

| ![Spreminjanje prioritete obveznostih](../img/SpreminjanjePrioritete.png) | 
|:--:| 
| *Osnovni in izjemni tok spreminjanja prioritete obveznostih* |

#### 13.Odstranevanje obveznostih

| ![Odstranevanje obveznostih](../img/Odstranevanje.png) | 
|:--:| 
| *Osnovni in izjemni tok 1 odstranjevanja obveznostih* |

| ![Odstranevanje obveznostih2](../img/odstranjevanje2.png) | 
|:--:| 
| *Izjemni tok 2 odstranjevanja obveznostih* |

#### 14.Dodajanje nosilcev predmeta

| ![Dodajanje nosilcev predmeta](../img/DodajanjeNosilcev.png) | 
|:--:| 
| *Osnovni in izjemni tok dodajanja nosilcev predmeta* |

#### 15.Dodajanje administratorskih dovoljenj pri predmetu

##### Profesor
| ![Dodajanje administratorskih dovoljenj pri predmetu](../img/DodajanjeAdmProf.png) | 
|:--:| 
| *Osnovni in izjemni tok dodajanje administratorskih dovoljenj pri predmetu - profesor* |

##### Asistent

| ![Dodajanje administratorskih dovoljenj pri predmetu -asistent](../img/DodajanjeAdmAsistent.png) | 
|:--:| 
| *Osnovni in izjemni tok dodajanje administratorskih dovoljenj pri predmetu - asistent* |

#### 16.Dodajanje rokov za vpis v višje letnike

| ![Dodajanje rokov za vpis v višje letnike](../img/DodajRok.png) | 
|:--:| 
| *Osnovni in izjemni tok dodajanja rokov za vpis v višje letnike* |

#### 17.Dodajanje dogodkov na fakulteti

| ![Dodajanje dogodkov na fakulteti](../img/DodajanjeDogodek.png) | 
|:--:| 
| *Osnovni in izjemni tok dodajanja dogodkov na fakulteti* |