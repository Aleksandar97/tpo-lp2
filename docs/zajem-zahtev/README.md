# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Martin Arsovski, Maja Nikoloska, Aleksandar Cuculoski, Bogdan Petrović |
| **Kraj in datum** | Ljubljana, 31.3.2019 |



## Povzetek projekta

Potrudili smo se narediti dobro organiziran zajem zahtev aplikacije StraightAs ki bo predvsem namenjena študentom. V uvodu je v kratkem razloženo kaj pomeni naša aplikacija in kaj bo počela.
Obstajajo različne funkcionalnosti ki jih bo naša aplikacija podpirala v namen da študentom poda prijazno sistematično organizacijo svojih obveznosti.
Uporabnike projekta smo razdelili v nekaj uporabniških vlog kot so študent, profesor, asistent ipd. Ker smo uporabljali več terminov pri izdelavi dokumenta, obstajati mora slovar vseh teh pojmov kjer so njihove definicije in pomeni ustrezno razloženi. 
Termini kot so obveznost, vaje in kolokviji so tudi v slovarju. Potem sledi seznam vseh funkcionalnih in nefunkcionalnih zahtev, pri čemer vsaki funkcionalni zahtevi bo dodeljena prioriteta ki bodo skupaj vključene v aplikacijo. Seveda moramo vse funkcionalnosti vizualno predstaviti in narediti dijagram uporabniških vlog in njihove medsebojne interakcije znotraj aplikacije.
Aplikacija bo komunicirala z informacijskih sistemom univerze, kar je predstavljeno v dijagramu.





## 1. Uvod


Prioriteta naše aplikacije je pomagati študentom organizirati čas za delo na obveznostih pri študiju. Vsebovala bo seznam stavkov ki jih je študent predhodno dodal in ki jih je planiral ta dnev narediti. Študent bo lahko brisal iz seznama, dodajal nove obveznosti v seznam in urejal prioriteto. Aplikacija bo podpirala naslednje funkcionalnosti:  

* Prijava v sistem. Vsak uporabnik mora imeti narejen svoj račun, s katerim lahko dostopa do vsebine sistema. Obstaja več različnih tipov uporabniškega računa (študent, profesor, asistent, urničar, delavec v referatu in skrbnik sistema).
* Uporabnik ki je lahko študent, profesor ali asistent lahko dobi seznam predmetov na katerih je vključen na fakulteti.
* Študent lahko dobi svoj "TO-DO seznam" po tem ko je dodal nekaj obveznosti ki jih je planiral narediti. Lahko ih tudi ureja po prioriteti in kronološki.
* Urničar po sprejemu seznama vseh uporabnikov lahko naredi urnik, ki bo dostopen vsem.
* Študent lahko urnik ki ga je dobil spreminja kakor mu ustreza.
* Asistenti imajo dovoljenja ureditve posameznih predmetov in dodajanje rokov sprotnih obveznosti. Ko asistent doda nekaj pri predmetu, vsebina bo vidna vsem študentom.
* Profesorji lahko spreminjajo roke sprotnih obveznosti.
* Asistenti na koncu kolokvijev, kvizov in domačih nalog objavijo rezultate.
* Študent bi lahko kot obveznost dodal termine govorilnih ur. Pred tem profesor mora objaviti čas v kateremu ga študent lahko obiskuje.
* Profesorji in asistenti lahko dodajo opis posameznih obveznosti.
* Študent bi lahko pri svojem "TO-DO seznamu" dodajal prioritete obveznostim in se bo seznam ponovno uredil.
* Študent lahko spreminja prioriteto obveznosti.
* Če je kakšna obveznost odpovedana ali se je študent odločil da je ne bo končal, je lahko odstrani iz seznama.
* Skrbnik sistema lahko doda nosilce predmetov(profesorje) in profesor bo imel polna dovoljenja glede urejanja predmeta.
* Skrbnik sistema lahko uporabniku poda administratorska dovoljenja pri predmetu.
* Delavec v referatu lahko objavi roke za vpise v višje letnike. Študenti po tem obvestilo lahko dodajo v seznam obveznostih.
* Delavec v referatu lahko doda različne dogodke na fakulteti v sistem.

Nekatere funkcionalnosti niso potrebne za delovanje aplikacije ustrezno idejam in se bomo odločili če ih omogočimo. Najbolj pomembne nam bojo funkcionalnosti ki imajo "Must have" prioriteto.  


## 2. Uporabniške vloge

* Študent 
    - pregled urnika
    - pregled obveznostih ki jih ima (domače naloge, kvizi, izzivi, kolokviji, izpiti)
    - sprememba urnika (če ima vaje za katere prisotnost ni obvezna, ali pa se je odločil da ne obiskuje nekatere vaje ali predavanja)
    - pregled rezultatov, doseženih točk na domačih nalogah, kvizih, izpitih, kolokvijih, ter ocene pri izpitih
    - dodajanje prioritete obveznostima
    - pregled rokov svojih obveznosti
    - dodajanje opisa za neko obveznost
    - odstranjevanje obveznosti (če ne namerava izvajati tо obveznost)
    - dodajanje termina govorilnih ur kot obveznost
    
* Profesor 
    - pregled predmetov na katerih je nosilec
    - dodajanje termina izpita kot obveznost
    - spreminjanje izpitnih rokov
    - dodajanje rezultatov pri izpitih
    - pregled rezultatov pri domačih nalog, kvizih, izzivov, kolokvijev, izpitov posameznega študenta
    - pregled in vnašanje končne ocene predmeta za posameznega študenta
    - dodajanje terminov govorilnih ur za posameznega študenta
    - dodajanje opisov domačih nalog, izzivov, kvizov, kolokvijev, izpitov
    - spreminjanje izpitnih rokov

* Asistent
    - pregled predmetov na katerih je asistent
    - dodajanje rokov za obveznosti (domače naloge, kvizi, izzivi, in kolokviji)
    - pregled in vnašanje rezultatov domačih nalog, kvizov, izzivov, kolokvijev 
    - spreminjanje rokov domačih nalog, kvivov, izzivov in kolokvijev
    - dodajanje terminov govorilnih ur za posameznega študenta
    - dodajanje opisov domačih nalog, izzivov, kvizov, kolokvijev

*  Urničari
    - dodajanje/spreminjanje urnika

* Delavec v referatu
    - objavljanje terminov vpisa v višji letnik
    - pregled urnika posameznega študenta
    - objavljanje zanimivih dogodkov na fakulteti

* Skrbnik sistema 
    - vzdržujenje šifrante študentov, asistentov in profesorjev 
    - dodajanje nekaterih administratorskih dovoljenj nosilcem predmeta (dodajanje rokov za domace naloge, kvize...)
    - dodajanje dovoljenja urničarjima za generiranje in spreminjanje urnika

## 3. Slovar pojmov


* **Predmet** - En izmed delov na katere se lahko proces učenja na fakulteti razdeli.
* **Semester** - Časovni interval ki označuje polovico študijskega leta.
* **Izpit** - Končno preverjanje znanj na koncu semestra pri katerem se določi študentova ocena.
* **Vpisna številka** - Identifikaciona enolična številka ki ju študent uporablja pri prijavi. 
* **Ocena** - Raven študentovega obvladanja gradiva, izražena v številkah od 5 do 10.
* **Nosilec predmeta** - Profesor, nekdo ki je odgovoren za izvajanje predmeta
* **Obveznost** - En stavek v študentovom seznamu nalog ki jih je planiral narediti.
* **Pregledna plošča** - Prvi zaslon ki ga uporabnik vidi. Vsebuje gumbe ki vodijo uporabnika do predmetov, obveznostih, ipd.
* **Vaje** - Način učenja zraven predavanj, kjer je poudarek običajno na praktičnem delu predmeta. 
* **Kolokvij** - Preverjanje znanj med tekom semestra ki je običajno predpogoj za opravljanje izpita.
* **Domače naloge** - Naloge ki so obvezne in se delajo samostojno.
* **Izpitni roki** - Časovni intervali v katerim študenti lahko opravljajo izpite.
* **Elektronski naslov** - Enolični identifikator vsakega uporabnika.
* **Urnik** - Vrstni red vaj in predavanj v delovnem dnevu.
* **Sprotne obveznosti** - Vse kar študent mora narediti da bi prišel do izpita: kolokviji, domače naloge in kvizi.
* **Administratorska dovoljenja** - Pravica uporabljanja vseh funkcij in urejanje aplikacije.
* **TO-DO seznam** - Seznam vseh obveznosti ki jih je študent dodal in planiral končati.
* **Govorilne ure** - Vsak profesor in asistent ima en termin v tednu v katerem študenti lahko pridejo na konsultacije in sprašujejo v zvezi nejasnostih pri predmetu.






## 4. Diagram primerov uporabe

![Diagram primerov uporabe](../img/tpo.png)




## 5. Funkcionalne zahteve


#### Naziv zahteve in povzetek funkcionalnosti

**1. Prijava v sistem** 

- Med prijavo asistent/prosefor/delavec v referatu vnese svoj mail in geslo.


##### Osnovni tok

###### Študent

1. Študent odpre aplikacijo in pride na začetni zaslon.
2. Študent izbere funkcionalnost *Prijava v sistem*
3. Študent vnese elektronski naslov/vpisno številko in geslo.
4. Študent pritisne gumb "Prijava".
5. Študent pride na svojo pregledno ploščo.

###### Profesor
1. Profesor odpre aplikacijo in pride na začetni zaslon.
2. Profesor izbere funkcionalnost *Prijava v sistem*
3. Profesor vnese elektronski naslov in geslo.
4. Profesor klikne gumb "Prijava".
5. Profesor pride na svojo pregledno ploščo.


###### Asistent
1. Asistent odpre aplikacijo in pride na začetni zaslon.
2. Asistent izbere funkcionalnost *Prijava v sistem*
3. Asistent vnese elektronski naslov in geslo.
4. Asistent klikne gumb "Prijava".
5. Asistent pride na svojo pregledno ploščo.

###### Urničar
1. Urničar odpre aplikacijo in pride na začetni zaslon.
2. Urničar izbere funkcionalnost *Prijava v sistem*
3. Urničar vnese elektronski naslov in geslo.
4. Urničar klikne gumb "Prijava".
5. Urničar pride na svojo pregledno ploščo.

###### Skrbnik sistema
1. Skrbnik sistema odpre aplikacijo in pride na začetni zaslon.
2. Skrbnik sistema izbere funkcionalnost *Prijava v sistem*
3. Skrbnik sistema vnese elektronski naslov in geslo.
4. Skrbnik sistema klikne gumb "Prijava".
5. Skrbnik sistema pride na svojo pregledno ploščo.

##### Izjemni tok
* Študent se ne more prijaviti in dostopati do pregledne plošče, ker se je izpisal ali je spremenil fakulteto.
* Profesor/asistent/urničar/delavec v referatu se ne more prijaviti in dostopati do pregledne plošče, ker ne dela več na fakulteti.

##### Pogoji
* Pogoj za prijavo za študenta je da mora biti vpisan na fakulteto.
* Pogoj za prijavo profesorja, asistenta, delavca v referatu in urničara je da morajo biti zaposleni na fakulteti.


##### Posledice
* Za vsakega prijavljenega uporabnika, se zapis njegovega nazadnjega dostopa do aplikacije spremeni.
* Za vsakega prijavljenega uporabnika, se poveča število trenutno prijavljenih uporabnikov.


##### Posebnosti

* Ni posebnosti.


##### Prioriteta

* Must have.


##### Sprejemni testi
* Prijava kot študent/profesor/delavec v referatu/urničar z napačnim elektronskim naslovom.
* Prijava kot študent/profesor/delavec v referatu/urničar z napačnim geslom.
* Prijava kot študent/profesor/delavec v referatu/urničar ki ni več na fakulteti.
* Prijava kot študent in dostop do pregledne plošče in seznama obveznosti.
* Prijava kot profesor/asistent in dostop do svojih predmetov.
* Prijava kot urničar in dostop do urnika.

***

**2.** **Pregled predmetov**
* Študent/Asistent lahko dobi seznam vseh predmetov na katerih je vpisan.
* Profesor lahko dobi seznam vseh predmetov na katerih je nosilec.

##### Osnovni tok

###### Študent

1. Študent po prijavi pride na začetno stran, in klikne na gumb "Moji predmeti".
2. Študentu se prikaže seznam vseh predmetov na katerih je prijavljen kot študent.
3. Študent predmete ureja po imenih.
4. Študent lahko dostopa do posameznega predmeta in pregleda obveznosti za ta predmet.

###### Profesor
1. Profesor po prijavi pride na začetno stran, in klikne na gumb "Moji predmeti".
2. Profesorju se prikaže seznam vseh predmetov na katerih je prijavljen kot nosilec.
3. Profesor predmete ureja po imenih/št. študentov.
4. Profesor lahko dostopa do posameznega predmeta in doda obveznost za ta predmet.

###### Asistent

1. Asistent po prijavi pride na začetno stran, in klikne na gumb "Moji predmeti".
2. Asistentu se prikaže seznam vseh predmetov na katerih je prijavljen kot asistent/delavec v referatu.
3. Asistent predmete ureja po imenih/št. študentov.
4. Asistent lahko dostopa do posameznega predmeta in doda obveznost za ta predmet.


##### Izjemni tok

* Študent/asistent/profesor ne more dostopati do predmetov, ker ih še ni izbral. Študent/asistent/profesor mora imeti vsaj en izbran predmet da lahko dostopa do seznama.
* Študent ne more dostopati do predmetov, ker ni prijavljen.
* Profesor ne more dostopati do predmetov, ker ni nosilec nobenega predmeta.
* Asistent ne more dostopati do predmetov, ker ni asistent/delavec v referatu pri nobenem predmetu.


##### Pogoji
* Da bi študent/profesor/asistent dostopal do seznama predmetov, mora biti prijavljen.
* Da bi študent dostopal do seznama predmetov, mora imeti vsaj eden izbran predmet.
* Da bi asistent dostopal do seznama predmetov, mora biti asistent pri vsaj enemu predmetu.
* Da bi profesor dostopal do seznama predmetov, mora biti nosilec vsaj enega predmeta.


##### Posledice
* Ni posledic.

##### Prioriteta
* Should have

##### Posebnosti
* Ni posebnosti.

##### Sprejemni testi

* Pregled predmetov kot neprijavljen uporabnik.
* Pregled predmetov kot študent ki ni izbral nobenega predmeta.
* Pregled predmetov kot asistent, ki ni asistent pri nobenemu predmetu.
* Pregled predmetov kot profesor, ki ni nosilec nobenega predmeta.
* Pregled predmetov kot študent/profesor/asistent s svojimi predmeti.

**3. Pregled "TO-DO" seznama**
* Študent lahko pregleda seznam dodanih obveznosti.
* Študent lahko obveznosti ureja po prioriteti.
* Študent lahko kronološki ureja dodane obveznosti.

###### Osnovni tok
1. Študent se prijavi v sistem.
2. Študent pride na začetno stran.
3. Študent klikne na gumb "Moj TO-DO seznam".
4. Študent pregleduje vse svoje dodane obveznosti.


##### Alternativni tok
* Študent se prijavi v sistem.
* Študent pride na začetno stran.
* Študent klkne na gumb "Moji predmeti".
* Študent izbere en predmet.
* Na strani predmeta študent lahko vidi vse obveznosti povezane s predmetom.


##### Izjemni tok
* Študent ne more dostopati do seznama svojih obveznosti ker ni prijavljen kot študent.
* Študent ne more dostopati do seznama ker ni dodal nobenih obveznosti.

##### Posledice
* Ni posledic.

##### Prioriteta 
* Must have

##### Posebnosti
* Ni posebnosti.

##### Sprejemni testi
* Dostop do seznama obveznosti kot neprijavljen uporabnik.
* Dostop do seznama obveznosti kot asistent/delavec v referatu/profesor/urničar.
* Dostop do seznama obveznosti kot študent brez dodanih obveznosti.
* Dostop do seznama obveznosti kot študent z dodanimi obveznosti.

 **4. Generiranje urnika**
* Ko urničar dobi seznam vseh uporabnikov, glede na število predmetov in asistentov lahko naredi urnik.
* Urničar naredi urnik posameznemu študentu.

##### Osnovni tok

###### Urničar 
 1. Urničar se prijavi v sistem (aplikacijo).
 2. Urničar klikne na gumb "Dodaj urnik".
 3. Sistem prikaze seznam predmetov.
 4. Urničar izbere predmet.
 5. Sistem prikaze izbiro vaj/predavanj za izbrani predmet in izbiro časa.
 6. Urničar izbere vaje/predavanja ali oboje in doda termin.
 7. Sistem vpraša uporabnika ali je končal z dodajanjem predmetov.
 8. Če je urničar končal, klikne gumb "Ja". Če ni končal klikne na gumb "Ne" in dodaja predmete.
 9. Sistem prikaže seznam študentov.
 10. Urničar izbira študente (za katere velja taj urnik).
 11. Sistem potrdi operacijo in prikaze začetni zaslon.

##### Izjemni tok
* Urničar ne more dodati urnika, ker ni prijavljen v sistem.
* Sistem ne potrjuje izvedeno (končano) operacijo, ker se je sesul.
* Urničar ne more dodati urnik, ker se študentu terminsko preklapajo vaje in predavanja.
* Urničar ne more dodati urnik študentu ker študent še nima aplikacijo.

##### Pogoji
* Da bi urničar dodal urnik mora biti prijavljen.
* Urničar mora imeti vnaprej določen urnik da bi ga dodal.
* Fakulteta mora dostaviti podatke urničaru za predmete in študente.
* Da bi urnik bil dodan študentu, študent mora biti prijavljen v sistemu.

##### Posledice 
* Vsak študent dobi svoj urnik.
* StraightAs vpošteva urnik (termine vaj in predavanj) pri izdelavi "TO-DO" seznama.


##### Prioriteta
* Must have 

##### Posebnosti 
* Urničar mora imeti  vnaprej določene podatke da bi generiral urnik.
 

##### Sprejemni testi
* Prijavi se kot urničar in dodaj urnik skupini študentov.
* Poskusi dodati urnik študentu ki še nima aplikacijo.


 **5. Spreminjanje urnika**
* Če se študent odloči da ne obiskuje nekatere vaje ali predavanja lahko spremeni svoj urnik.
* Urničar lahko spremeni urnik. Primer: če je profesor spremenil termin predavanja, ali če študenti zamenjajo vaje med sabo.

##### Osnovni tok

###### Urničar 

 1. Urničar se prijavi v sistem (aplikacijo).
 2. Urničar klikne na gumb "Spremeni urnik".
 3. Sistem prikaze seznam študentov.
 4. Urničar izbira študente (katerim želi spremeniti urnik).
 5. Sistem prikaže seznam predmetov ki jih ima ta študent.
 6. Urničar izbere predmet.
 7. Sistem prikaže izbiro vaj/predavanj za ta predmet in izbiro časa.
 8. Urničar izbere vaje/predavanja, ali oboje in spremeni termin. 
 9. Sistem potrdi spremembo in prikaže začetni zaslon.

###### Študent 
 1. Študent se prijavi v sistem (aplikacijo).
 2. Študent klikne na gumb "Spremeni svoj urnik".
 3. Sistem prikaže seznam predmetov.
 4. Študent izbere predmet.
 5. Sistem prikaže izbiro vaj/predavanja za ta predmet.
 6. Študent izbere vaje/predavanja ali oboje.
 7. Študent klikne gumb "Odstrani obveznost".
 8. Sistem potrdi spremembo in prikaže začetni zaslon.

##### Alternativni tok 

###### Urničar 

 1. Urničar se prijavi v sistem (aplikacijo).
 2. Urničar klikne na gumb "Zamenjaj obveznost".
 3. Sistem prikaže seznam študentov.
 4. Urničar selektira dva študenta (za katere želi spremeniti urnik).
 5. Sistem prikaze seznam predmetov ki jih imata oba študenta.
 6. Urničar izbere predmet.
 7. Sistem prikaže izbiro vaj/predavanj za ta predmet.
 8. Urničar izbere vaje/predavanja, ali oboje. 
 9. Sistem potrdi spremembo in prikaže začetni zaslon.

###### Študent 
 1. Študent se prijavi v sistem (aplikacijo).
 2. Študent klikne na gumb "Moji predmeti".
 3. Sistem prikaze seznam predmetov študenta.
 4. Študent izbere predmet.
 5. Sistem prikaže izbiro vaj/predavanj za ta predmet.
 6. Študent klikne gumb "Odstrani obveznost" in izbere vaje/predavanja ali oboje.
 7. Sistem potrdi spremembo, spremeni urnik študenta in prikaže začetni zaslon.

##### Izjemni tok
* Urničar ne more spremeniti urnika, ker ni prijavljen v sistem.
* Sistem ne potrjuje izvedeno (končano) operacijo, ker se je sesul.
* Urničar ne more spremeniti urnik študentu ker študent še nima aplikacijo.
* Študent ne more spremeniti svoj urnik, ker ni priavljen v sistem.

##### Pogoji
* Da bi urničar spremenil urnik mora biti prijavljen.
* Da bi urnik bil dodan študentu, študent mora biti prijavljen v sistem.
* Da bi študent lahko spremenil svoj urnik mora biti prijavljen.

##### Posledice 
* Študent dobi spremenjen urnik.
* StraightAs vpošteva spremembo urnika in izdela novo "TO-DO" listo.

##### Prioriteta
* Must have 

##### Posebnosti 
Ni posebnosti.
 

##### Sprejemni testi
* Prijavi se kot urničar, izberi predmet in spremeni termin izvajanja predmeta.
* Prijavi se kot študent, izberi predmet, vaje/predavanja tega predmeta in odstrani iz urnika.


**6. Objavljanje rokov sprotnih obveznosti**
   - Asistenti lahko na pregledni plošči predmeta objavijo roke za kolokvije, kvize, domače naloge in izzive.
    - Profesor lahko na pregledni plošči predmeta objavi roke za izpite.
    - Delavec v referatu lahko doda roke za izpite.
 
##### Osnovni tok

###### Profesor 

 1. Profesor se prijavi v sistem (aplikacijo).
 2. Profesor klikne gumb "Dodaj izpitni rok".
 3. Sistem prikaže predmete pri katerih je profesor nosilec.
 4. Profesor izbere en predmet.
 5. Sistem prikaže izbiro časa.
 6. Profesor doda termin za to obveznost.
 7. Sistem potrdi dodan rok sprotne obveznosti.

###### Asistent 
 1. Asistent se prijavi v sistem (aplikacijo).
 2. Asistent klikne na gumb "Dodaj obveznost".
 3. Sistem prikaže predmete pri katerih je asistent nosilec.
 4. Asistent izbere en predmet.
 5. Sistem prikaže izbiro kolokvijev/kvizov/izzivov/domačih nalog.
 6. Asistent izbere eno možnost med ponudenimi.
 7. Sistem prikaže izbiro časa.
 8. Asistent doda termin za to obveznost.
 7. Sistem potrdi dodan rok sprotne obveznosti.

##### Izjemni tok
* Profesor/asistent ne more dodati obveznost ker ni nosilec tega predmeta.
* Profesor ne more dodati izpitni rok, ker v tem terminu že obstaja izpitni termin drugega predmeta.
* Asistent ne more dodati rok za kolokvij, ker že obstaja kolokvij v istem terminu drugega predmeta.

##### Pogoji
* Profesor/asistent mora biti prijavljen v sistem.
* Profesor/asistent mora biti nosilec predmeta da bi dodal rok za neko obveznost.
* Študent mora imeti določeni predmet, da bi dobil njegovo obveznost.
* Študent mora biti prijavljen na izpit da bi dobil obveznost ki jo je dodal profesor.

##### Posledice 
* Študent dobi novo obveznost.
* StraightAs vpošteva dodano obveznost in je doda v "TO-DO" listo študenta.

##### Prioriteta
* Must have 

##### Posebnosti 
* Ni posebnosti.
 

##### Sprejemni testi
* Prijavi se kot asistent, izberi predmet in dodaj termin za kolokvij/kviz/izziv/domačo nalogo.
* Prijavi se kot profesor, izberi predmet, in dodaj termin za izpit kot obveznost.
 


 **7. Spreminjanje rokov sprotnih obveznosti**
    - Nosilci predmetov lahko spreminjajo roke za neko obveznost.
    
##### Osnovni tok

###### Profesor 

 1. Profesor se prijavi v sistem (aplikacijo).
 2. Profesor klikne gumb "Spremeni izpitni rok".
 3. Sistem prikaže predmete na katerih je profesor nosilec in imajo dodan rok izvajanja.
 4. Profesor izbere en predmet.
 5. Sistem prikaže novo izbiro časa.
 6. Profesor spremeni termin za to obveznost.
 7. Sistem potrdi spremembo.

###### Asistent 
 1. Asistent se prijavi v sistem (aplikacijo).
 2. Asistent klikne na gumb "Spremeni rok obveznosti".
 3. Sistem prikaze predmete na katerih je asistent nosilec.
 4. Asistent izbere en predmet.
 5. Sistem prikaže izbiro kolokvijev/kvizov/izzivov/domačih nalog ki že imajo dodan rok izvajanja.
 6. Asistent izbere eno možnost med ponudenimi.
 7. Sistem prikaže novo izbiro časa.
 8. Asistent doda termin za to obveznost.
 7. Sistem potrdi spremenjen rok sprotne obveznosti.

##### Alternativni tok 

###### Profesor 

 1. Profesor se prijavi v sistem (aplikacijo).
 2. Profesor klikne gumb "Moji predmeti".
 3. Profesorju se prikaže seznam vseh predmetov na katerih je prijavljen kot nosilec.
 4. Profesor izbere en predmet.
 5. Profesor izbere opcijo "Spremeni izpitni rok".
 6. Sistem prikaže novo izbiro časa.
 6. Profesor spremeni termin za to obveznost.
 7. Sistem potrdi spremembo.

###### Asistent 

 1. Asistent se prijavi v sistem (aplikacijo).
 2. Asistent klikne gumb "Moji predmeti".
 3. Asistentu se prikaže seznam vseh predmetov na katerih je prijavljen kot asistent.
 4. Asistent izbere en predmet.
 5. Asistent izbere opcijo "Spremeni rok obveznosti".
 6. Sistem prikaže izbiro kolokvijev/kvizov/izzivov/domačih nalog ki že imajo dodan rok izvajanja.
 7. Asistent izbere eno opcijo od ponudenih.
 8. Sistem prikaže novo izbiro časa.
 9. Asistent doda termin za to obveznost.
 10. Sistem potrdi spremenjen rok sprotnih obveznosti.


##### Izjemni tok
* Profesor/asistent ne more spremeniti rok za obveznost ker ni nosilec tega predmeta.
* Profesor/asistent ne more spremeniti rok za obveznost ker rok pred tem ni bil dodan.
* Profesor ne more spremeniti izpitni rok, ker v tem terminu že obstaja izpitni termin drugega predmeta.
* Asistent ne more spremeniti rok kolokvija, ker že obstaja kolokvij v istem terminu drugega predmeta.

##### Pogoji
* Profesor/asistent mora biti prijavljen v sistem.
* Profesor/asistent mora biti nosilec predmeta da bi spremenil rok za neko obveznost.
* Študent mora imeti ta predmet, da bi dobil spremenjen rok obveznosti.
* Študent mora biti prijavljen na izpit da bi dobil spremenjen izpistni rok.

##### Posledice 
* Študent dobi spremenjen rok obveznosti.
* StraightAs vpošteva spremenjen rok obveznosti in spremeni "TO-DO" listo študenta.

##### Prioriteta
* Must have 

##### Posebnosti 
* Ni posebnosti.
 

##### Sprejemni testi
* Prijavi se kot asistent, izberi predmet in spremeni termin za kolokvij/kviz/izziv/domačo nalogo.
* Prijavi se kot profesor, izberi predmet, in spremeni termin izpita. 

**8. Objava rezultatov**
   - Po poteku roka asistenti lahko objavljajo rezultate kvizov, domačih nalog, izzivov ipd.
    - Po končanemu kolokviju, asistenti objavijo rezultate kolokvija.
    - Po končanemu izpitu, profesorji objavijo rezultate izpita.
##### Osnovni tok

###### Profesor 

 1. Profesor se prijavi v sistem (aplikacijo).
 2. Profesor izbere predmet.
 3. Profesor izbere izpitni rok.
 4. Profesor klikne gumb "Dodaj rezultate".
 5. Profesor doda rezultate za vsakega študenta.
 6. Sistem potrdi.

###### Asistent 
 1. Asistent se prijavi v sistem (aplikacijo).
 2. Asistent izbere predmet.
 3. Asistent izbere dogodek.
 4. Asistent klikne na gumb "Dodaj rezultate".
 5. Asistent doda rezultate za vsakega študenta.
 6. Asistent izbere en predmet.
 7. Sistem potrdi.

##### Izjemni tok
* Profesor/asistent ne more dodajati rezultate za obveznost ker ni nosilec tega predmeta.
* Profesor/asistent ne more spremeniti rezultate za obveznost ker ni nosilec tega predmeta.

##### Pogoji
* Profesor/asistent mora biti prijavljen v sistem.
* Profesor/asistent mora biti nosilec predmeta da bi dodajal rezultate.
* Študent mora imeti ta predmet, da ima vpogled v rezultate.

##### Posledice 
* Rezultati obveznosti so objavljeni.

##### Prioriteta
* Could have

##### Posebnosti 
Ni posebnosti.
 

##### Sprejemni testi
* Prijavi se kot asistent, izberi predmet in dodaj rezultate za kolokvij/kviz/izziv/domačo nalogo.
* Prijavi se kot profesor, izberi predmet, in dodaj rezultate izpita. 


 **9. Dodajanje termina za govorilne ure**
   - Študent lahko kot obveznost doda termin govorilnih ur.
   - Nosilci predmetov lahko dodajo dogovorjene ternime govorilnih ur.
###### Osnovni tok

###### Profesor/Asistent

 1. Profesor/asistent se prijavi v sistem (aplikacijo).
 2. Profesor/asistent izbere predmet.
 3. Profesor/asistent klikne gumb "Dodaj termin za govorilne ure".
 4. Sistem prikaže 2 polji za dan in uro od/do.
 5. Profesor/asistent izbere dan in uro.
 6. Profesor/asistent klikne gumb "Dodaj".
 7. Sistem potrdi spremembo.
  
###### Študent

 1. Študent se prijavi v sistem.
 2. Študent pride na pregledno ploščo.
 3. Študent klikne gumb "Moj TO-DO seznam".
 4. Študent klikne gumb "Dodaj".
 5. Študent izbere dogodek govorilne ure, predmet in asistent/profesor.
 6. Študent klikne gumb "Dodaj".
 7. Sistem potrdi.


##### Izjemni tok
* Profesor/Asistent ne more dodajati govorilne ure ker ni nosilec tega predmeta.
* Študent ne more prijaviti govorilne ure ki ne obstajajo.

##### Pogoji
* Profesor/asistent mora biti prijavljen v sistem.
* Profesor/asistent mora biti nosilec predmeta da bi dodal govorilne ure.
* Študent mora imeti ta predmet, da lahko pregleda govorilne ure.
* Študent mora imeti ta predmet, da doda govorilne ure v TO-DO seznam.

##### Posledice 
* Profesor/Asistent dobi termin govorilnih ur in če ponovno doda dan in uro, termin za govorilne ure se spremeni.
* Student ima govorilne ure v TO-DO seznamu.

##### Prioriteta
* Should have 

##### Posebnosti 
Ni posebnosti.
 

##### Sprejemni testi
* Prijavi se kot profesor/asistent, izberi predmet in dodaj termin za govorilne ure.
* Prijavi se kot študent, izberi govorilne ure v TO-DO seznam in dodaj termin za govorilne ure v TO-DO seznam.

 **10. Dodajanje opis obveznostih**
    - Nosilcev predmeta lahko dodajo nek opis za kviz, izziv, domačo nalogo ipd.
##### Osnovni tok

###### Profesor/Asistent 

 1. Profesor/Asistent se prijavi v sistem (aplikacijo).
 2. Profesor/Asistent izbere predmet.
 3. Profesor/Asistent izbere obaveznost.
 4. Profesor/Asistent klikne gumb "Dodaj opis".
 5. Profesor/Asistent doda novi opis obaveznosti.
 6. Sistem potrdi.

##### Izjemni tok
* Profesor/Asistent ne more dodajati opis za obveznost ker ni nosilec tega predmeta.
* Profesor/Asistent ne more spremeniti opis za obveznost ker ni nosilec tega predmeta.

##### Pogoji
* Profesor/Asistent mora biti prijavljen v sistem.
* Profesor/Asistent mora biti nosilec predmeta da doda opis.
* Študent mora imeti ta predmet, da pregleda opis.

##### Posledice 
* Dodan je opis za obveznos.

##### Prioriteta
* Could have 

##### Posebnosti 
Ni posebnosti.
 

##### Sprejemni testi
* Prijavi se kot Profesor/Asistent, izberi predmet in dodaj opis za obaveznost.

 **11. Dodajanje prioritete obveznostih**
   - Študent lahko uredi svoje obveznosti po prioritetah.
    
##### Osnovni tok
* Študent se prijavi v sistem
* Študent pride na pregledno ploščo
* Študent klikne gumb "Moj TO-DO seznam"
* Študent lahko vidi seznam vseh obveznosti ki jih je predhodno dodal
* Študent lahko klikne na eno od obveznosti in odkljuka opcijo "zelo pomembno(rdeče)", "osrednje pomembno(rumeno)", "ni pomembno(zeleno)".
* Obveznosti se posodobe in se na zaslonu prikaže ponovno urejen seznam po prioritetah.


##### Izjemni tok
* Študent ne more dodati prioriteto posamezne obveznosti ker je rok obveznosti potekel.
* Študent ne more dodati prioriteto posamezne obveznosti ker ni dodal nobene v "Moj TO-DO seznam".

##### Pogoji
* Mora biti prijavljen kot študent.
* Mora imeti vsaj eno dodano obveznost.
* Lahko izbere samo eno izmed tri mogoče prioritete pri posamezni obveznosti.


##### Posledice
* Dodajanje prioritete posamezni obveznosti.
* Sprememba urejenosti seznama obveznosti.

##### Prioriteta

* Should have.

##### Sprejemni testi

* Dodaj prioriteto obvezosti ki je potekla.
* Dodaj prioriteto obveznosti ki še vedno velja.
* Dodaj prioriteto vsem obveznostim.


 
 **12. Spreminjanje prioritete obveznostih**
    - Študent lahko spremeni svojo prioriteto obveznostih.
    
##### Osnovni tok
* Študent se prijavi v sistem
* Študent pride na pregledno ploščo
* Študent klikne gumb "Moj TO-DO seznam"
* Študent lahko vidi seznam vseh obveznosti ki jih je predhodno dodal
* Študent lahko klikne na eno od obveznosti ki jih je predhodno dodal prioriteta in odkljuka opcijo rdeče, rumeno, zeleno (opcija ki ni momentalno izbrana).
* Obveznosti se posodobe in se na zaslonu prikaže ponovno urejen seznam po prioritetah.


##### Izjemni tok
* Študent ne more dodati prioriteto posamezne obaveznosti ker je rok obveznosti potekel.
* Študent ne more dodati prioriteto posamezne obaveznosti ker ni dodal nobene v "Moj TO-DO seznam".

##### Pogoji
* Mora biti prijavljen kot študent.
* Mora imeti vsaj eno dodano obveznost.
* Obveznost že ima prioriteto.
* Lahko izbere samo eno izmed dveh mogočih prioritetah pri posamezni obveznosti.


##### Posledice
* Dodajanje nove prioritete posamezni obveznosti.
* Sprememba urejenosti seznama obveznosti.

##### Prioriteta

* Should have.

##### Posebnosti 
Ni posebnosti.

##### Sprejemni testi

* Spremeni prioriteto obvezosti.

 **13. Odstranevanje obveznostih**
    - Študent lahko odstrani neko obveznost. Primer : ne namerava izvajati to obveznost ker ni obvezna.
    
##### Osnovni tok

###### Študent

* Študent se prijavi v sistem.
* Študent pride na pregledno ploščo.
* Študent klikne gumb "Moj TO-DO seznam".
* Študent lahko vidi seznam vseh obveznosti ki jih je predhodno dodal.
* Študent lahko klikne na obveznost ki jo želi odstraniti.
* Študent klikne gumb "Odstrani to obveznost".
* Sistem potrdi izvedeno operacijo.
* Obveznosti se posodobe in se na zaslonu ponovno prikaže seznam brez te obveznosti.

##### Izjemni tok
* Študent ne more odstraniti obveznost ker sistem ne potrdi izvedeno operacijo(se je sesul).
* Študent ne more odstraniti obveznost ker je rok obveznosti že potekel.

##### Pogoji
* Študent mora biti prijavljen v sistem da bi odstranil obeznost.
* TO-DO seznam študenta ne sme biti prazen.

##### Posledice 
* Odstranevanje posamezne obveznosti.
* Spremenjen seznam obveznosti.

##### Prioriteta
* Should have 

##### Posebnosti 
* Ni posebnosti.
 

##### Sprejemni testi
* Odstrani obveznost kateri rok je potekel.
*  Odstrani obveznost kateri rok ni potekel.



 **14. Dodajanje nosilcev predmeta**
    - Skrbnik sistema lahko dodeli nosilce posameznega predmeta.
 
##### Osnovni tok

###### Skrbnik

* Skrbnik se prijavi v sistem .
* Skrbnik pride na pregledno ploščo.
* Skrbnik klikne gumb "Seznam nosilcev predmetov".
* Sistem prikaže seznam nosilcev predmetov in predmet katerega so nosilci.
* Skrbnik klikne gumb "Dodaj nosilec predmeta".
* Skrbnik izbere predmet od seznama predmetov.
* Skrbnik izbere nosilec od seznama profesorjev in asistentov.
* Skrbnik klikne gumb "Shrani".
* Sistem potrdi izvedeno operacijo.
* Seznam se posodobi in se na zaslonu ponovno prikaže seznam vključno z novim nosilcem predmeta.

##### Izjemni tok
* Skrbnik ne more dodati nosilca predmeta ker asistenta/profesora ni v seznamu.
* Skrbnik ne more dodati nosilca predmeta ker predmeta ni v seznamu.

##### Pogoji
* Profesor/asistent mora biti v seznamu.
* Predmet mora biti v seznamu.

##### Posledice 
* Dodajanje novega nosilca predmeta.
* Spremenjen seznam nosilcev predmetov.

##### Prioriteta
* Must have 

##### Posebnosti 
* Ni posebnosti.
 

##### Sprejemni testi
* Skrbnik doda nosilca predmeta.

 **15. Dodajanje administratorskih dovoljenj pri predmetu**
   - Skrbnik sistema lahko poda administratorska dovoljenja nosilcem predmetov. Administratorska dovoljenja vključujejo dodajanje kolokvijev, kvizov ipd.
    

##### Osnovni tok

###### Skrbnik

* Skrbnik se prijavi v sistem .
* Skrbnik pride na pregledno ploščo.
* Skrbnik klikne gumb "Seznam nosilcev predmetov".
* Sistem prikaže seznam nosilcev predmetov in predmet katerega so nosilci.
* Skrbnik izbere "Nosilec predmeta".
* Skrbnik odkljuka opcijo "Dodaj administratorska dovoljenja".
* Skrbnik klikne gumb "Shrani".
* Sistem potrdi izvedeno operacijo.
* Seznam se posodobi in se na zaslonu ponovno prikaže seznam predmetov in nosilcev predmetov.

##### Izjemni tok
* Skrbnik ne more dodati administratorska dovoljenja nosilcu predmeta ker asistenta/profesora ni v seznamu.
* Skrbnik ne more dodati administratorska dovoljenja nosilcu predmeta ker asistent/profesor že ima administratorska dovoljenja.

##### Pogoji
* Profesor/asistent mora biti v seznamu.
* Profesor/asistent nima administratorskih dovoljenj.

##### Posledice 
* Dodajanje administratorskih dovoljenja nosilcu predmeta.


##### Prioriteta
* Must have 

##### Posebnosti 
* Ni posebnosti.
 

##### Sprejemni testi
*  Skrbnik doda administratorska dovoljenja nosilcu predmeta.
*  Skrbnik doda administratorska dovoljenja nosilcu predmeta, ki že ima administratorska dovoljenja.

 **16. Dodajanje rokov za vpis v višje letnike**
    - Delavec v referatu lahko kot obveznost vsakega študenta doda roke za vpis v višji letnik

##### Osnovni tok

###### Delavec v referatu

* Delavec v referatu se prijavi v sistem .
* Delavec v referatu pride na pregledno ploščo.
* Delavec v referatu klikne gumb "Dodaj rok za vpis v višji letnik".
* Sistem prikaže dve polji za dan (od-do).
* Delavec v referatu izbere dane.
* Skrbnik klikne gumb "Shrani".
* Sistem potrdi izvedeno operacijo.
* Na zaslonu se prikaže dodan rok za vpis v višji letnik.

##### Izjemni tok
* Delavec v referatu ne more dodati rokov za vpis v višje letnike ker ni prijavljen v sistem.
* Delavec v referatu ne more dodati rok za vpis v višje letnike ker rok je že objavljen.

##### Pogoji
* Delavec v referatu mora biti prijavljen v sistem.
* Profesor/asistent nima administratorska dovoljenja.

##### Posledice 
* Dodajanje rokov za vpis v višje letnike.


##### Prioriteta
* Should have 

##### Posebnosti 
* Ni posebnosti.
 

##### Sprejemni testi
*  Delavec v referatu doda rok za vpis v višje letnike.
*  Delavec v referatu doda rok za vpis v višje letnike, tudi če je že objavljen.

  **17. Dodajanje dogodkov na fakulteti**
    - Delavec v referatu lahko doda nek dogodek kot obveznost študenta, če se je študent za ta dogotek prijavil (primer Dragonhack). 
    
##### Osnovni tok

###### Delavec v referatu

* Delavec v referatu se prijavi v sistem .
* Delavec v referatu pride na pregledno ploščo.
* Delavec v referatu klikne gumb "Dodaj dogodek".
* Sistem prikaže štiri polja za ime dogodka, mesto dogodka, dan dogodka (od-do) in ura dogodka(od-do).
* Delavec v referatu izbere ime, dan in uro dogodka.
* Skrbnik klikne gumb "Shrani".
* Sistem potrdi izvedeno operacijo.
* Sistem doda dogodek kot obveznost študentom ki se je prijavil na dogodek.
* Na zaslonu se prikaže dodan dogodek.

##### Izjemni tok
* Delavec v referatu ne more dodati dogodek ker že obstaja nek dogodek v tem terminu in mestu.

##### Pogoji
* Delavec v referatu mora biti prijavljen v sistem.

##### Posledice 
* Dodajanje dogodkov.


##### Prioriteta
* Should have 

##### Posebnosti 
* Ni posebnosti.
 

##### Sprejemni testi
*  Delavec v referatu doda dogodek.



## 6. Nefunkcionalne zahteve

### Zahteve izdelka
* **Uporabnost**
    - Sistem omogoča sočasno uporabnost najmanj 4000 uporabnikov (Študent/Profesor/Asistent).
    - Čas odzivnosti za izvajanje operacij je < 5s za največjo delovno obremenitev.
    - Sistem omogoča izvajanje 1500 operacij v sekundi.
    - Sistem je vedno na voljo.


* **Varnost**
    - Študent lahko pogleda samo svoj urnik, svoje obveznosti, svoje rezultate.
    - Profesor in asistent imajo pravice vnašati podatke(izpitni roki, rezultati, ocene, čas za govorilne ure..) samo za predmet ki ga izvajajo.
    - Profesor in asistent nimajo dostopa do podatkov predmeta katerega ne izvajajo.
    - Študent nima več dostopa do aplikacije, če je končal z študij.

* **Zanesljivost**
    - Pri neuspešnem dodajanju novega izpitnega roka, prijavljanje na izpit ali druge neuspešne spremembe v podatkovni bazi, sistem vrne stanje v podatkovni bazi preden so spremembe nastale.

* **Izvedba**
    - Čas nalaganja strani ni daljši od 5s.

* **Razpoložljivost**
    - Dodajanje izpitnih rokov, objavljanje rezultatov in dodajanje obveznosti ne vpliva na razpoložljivost strani.
    - Vzdrževalni poseg ne sme trajati več kot eno uro.

* **Razširljivost**
    - Sistem omogoča sočasno uporabnost najmanj 4000 uporabnikov brez rušenja sistema.

### ORGANIZACIJSKE ZAHTEVE

*  Uporablja se Scrum metoda.
*  Uporabniki se morajo identificirati z svojim elektronskim naslovo, in geslom, ki so ga dobili od univerze, da bi uporabljali sistem.  

### ZUNANJE ZAHTEVE

* Sistem identifikacije uporabnikov.
* Sistem generiranja dogodkov.


## 7. Prototipi vmesnikov

1.Začetna
![Začetna](../img/Wireframe/1.png)
2.Login
![Login](../img/Wireframe/2.png)
3.Začetna(Loggedin)
![Začetna logged](../img/Wireframe/3.png)
4.Urnik
![Urnik](../img/Wireframe/4.png)
5.Moj TO-DO
![TO-DO](../img/Wireframe/5.png)
6.Predmet
![Predmet](../img/Wireframe/6.png)
7.Dodaj izpitni rok
![Dodaj izpitni rok](../img/Wireframe/7.png)
8.Dodaj rezultate asistent
![Asistent rezultate](../img/Wireframe/16.png)
9.Dodaj rezultate kot profesor
![Profesor rezultate](../img/Wireframe/8.png)
10.Dodaj obaveznost
![Dodaj obaveznost](../img/Wireframe/9.png)
11.Dodaj v urnik
![Dodaj v urnik](../img/Wireframe/11.png)
12.Dodaj v urnik študentu
![Dodaj v urnik](../img/Wireframe/12.png)
13.Dodaj nosilec + administratorska dovoljenja
![Dodaj nosilec](../img/Wireframe/13.png)
14.Dodaj termin za govorilne ure
![Dodaj govorilne ure](../img/Wireframe/14.png)
15.Vpis v višje letnike
![višje letnike](../img/Wireframe/15.png)

### Sistem za identifikacija uporabnikov

Povezava med StraightAs in sistemom univerzitetne službe, prek katere dostopamo do podatkov študentov in zaposlenih. Sistem univerze deluje s funkcijo *uporabniki(fakulteta, vrstaUporabnika)* , ki sprejema id fakultete in id vrste uporabnika. Rezultat dobimo v obliki CSV datoteke, v kateri vsaka vrstica predstavlja enega uporabnika. Vsak uporabnik ki je študent univerze je opisan s podatki v obliki:  

* id
* ime
* priimek
* email
* geslo
* vpisna številka
* letnik
* datum rojstva v obliki DD.MM.YY

Vsak uporabnik ki je asistent ali profesor na univerzi je opisan s podatki v obliki:  

* id
* ime
* priimek
* email
* geslo
* številka zaposlenega
* predmeti
* datum rojstva v obliki DD.MM.YY

Vsak uporabnik ki je urničar ali delavec v referatu univerze je opisan s podatki v obliki:  

* id
* ime
* priimek
* email
* geslo
* številka zaposlenega
* datum rojstva v obliki DD.MM.YY

Skrbnik je opisan s podatke v obliki:  

* id
* ime
* priimek
* email
* geslo 
* datum rojstva v obliki DD.MM.YY

### Sistem za generiranje dogodkov

Povezava med StraightAs in sistemom za generiranje dogodkov, preko katere dostopamo do dogodkov, ki se izvajajo na univerzi. Sistem deluje s funkcijo *dogodki(fakulteta)*, ki sprejema id fakultete. Rezultat dobimo v obliki CSV datoteke, v kateri vsaka vrstica predstavlja en dogodek. Vsak dogodek je opisan s podatki v obliki:  

* id
* ime
* datum 
* ura
* kraj